
import cv2
import numpy as np
from datetime import datetime
import os
#img = cv2.imread("test.png")
def image_obj(image):
    img = cv2.imread(image)
    blurred = cv2.blur(img, (3,3))
    canny = cv2.Canny(blurred, 10, 200)

    ## find the non-zero min-max coords of canny
    pts = np.argwhere(canny>0)
    y1,x1 = pts.min(axis=0)
    y2,x2 = pts.max(axis=0)

    ## crop the region
    cropped = img[y1:y2, x1:x2]
    cv2.imwrite("/home/raghu/Downloads/idvoter/"+str(datetime.now())+'.jpg', cropped)

    tagged = cv2.rectangle(img.copy(), (x1,y1), (x2,y2), (0,255,0), 3, cv2.LINE_AA)
# for root, dirs, files in os.walk("/home/raghu/Downloads/voteid"):
#         for filename in files:
#              abc=image_obj(root+"/"+filename)
# cv2.imshow("tagged", tagged)
# cv2.waitKey()
image_obj('/home/raghu/Downloads/IMG_20190914_165229_Bokeh.jpg')