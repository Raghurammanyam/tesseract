import base64
import requests
import io
import os
import json
import re
import datetime
from datetime import date,time
from os.path import join
from difflib import get_close_matches
import dateparser
from collections import defaultdict,OrderedDict

from googletrans import Translator
translator = Translator()


def detect_text(image_file):
 #   try:
    with open(image_file, 'rb') as image:
        base64_image = base64.b64encode(image.read()).decode()
    url = 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyAOztXTencncNtoRENa1E3I0jdgTR7IfL0'
    header = {'Content-Type': 'application/json'}
    body = {
        'requests': [{
            'image': {
                'content': base64_image,
            },
            'features': [{
                'type': 'DOCUMENT_TEXT_DETECTION',
                'maxResults': 100,
            }],
            "imageContext":{
            "languageHints":["en-t-iO-handwrit"]
            }
        }]
    }

    response = requests.post(url, headers=header, json=body).json()
   
    text = response['responses'][0]['textAnnotations'][0]['description'] if len(response['responses'][0]) > 0 else ''
    print(text)
    translated=translator.translate(text)
    trans_en=translated.text
    print(trans_en)
    trans_en_split=str(trans_en).split('\n')
    trans_en_split =[x for x in trans_en_split if 'religious' not in x if 'Buddhism' not in x  if 'Future' not in x if x != '-' if x!= '.']
    id = re.findall('[0-9]{1} [0-9]{4} [0-9]{5} [0-9]{2} [0-9]{1}|[0-9]{5} [0-9]{5} [0-9]{2} [0-9]{1}|[0-9]{1} [0-9]{4} [0-9]{8}|[0-9]{1} [0-9]{4} [0-9]{5} [0-9]{2}|[0-9]{10} [0-9]{3}|[0-9]{1} [0-9]{3} [0-9]{3} [0-9]{3} [0-9]{3}|[0-9]{1} [0-9]{12}|[0-9]{5} [0-9]{5} [0-9]{3}',text)
    uid = ''
    try:
        if len(id)>0:
            id_no=re.sub(' ','',id[0])
            uid = id_no[0]+' '+id_no[1:5]+' '+id_no[5:10]+' '+id_no[10:12]+' '+id_no[12]
            print("idno:",id[0])
    except:
       uid = '' 
    date_of_birth = re.findall('[0-9]{1,2}\.[A-Za-z]{3}\.[0-9]{4}|[0-9]{1,2} [A-Za-z]{3} [0-9]{4}|[0-9]{1,2} [A-Za-z]{3}\.[0-9]{4}|[0-9]{1,2}\.[A-Za-z]{3} [0-9]{4}|[0-9]{1,2} [A-Za-z]{3}\. [0-9]{4}|[0-9]{1,2} [A-Za-z]{3}\, [0-9]{4}',text)
    birth_date =''
    if len(date_of_birth)>0:
        #print("date",re.sub('. ',' ',date_of_birth[0]))
        b_date=date_of_birth[0]
        birth_date=b_date.replace('.','')
        birth_date=birth_date.replace(',','')
    print("dateofbirth:",date_of_birth)
    block=str(text).split('\n')
    name =''
    last_name=''
    for x in block:
        if 'Name' in x or 'Namo' in x or 'Nume' in x or  'Miss' in x or 'Mr' in x or 'Nome' in x:
            abc=block.index(x)           
            name=re.sub('Name|Namo|Nume|Nane|Ne|Nome','',x)
            break
    for y in block:
        if 'Last name' in y or 'Last' in y or 'Lext' in y:
            bac=block.index(y)
            print('lastgg')
            if 'Late' not in y  or 'Label' not in y:
                last_name=re.sub('Last name|Last|Lame|name|Middle|Name','',y)
                print(last_name)
    if name=='' and last_name !='':
        print('nameiden')
        name=re.sub('Name|Namo|Nume|Nane','',block[bac-1])
    if last_name=='' and name !='':
        last_name=re.sub('Last name|Last|Lame|name|Middle|Name','',block[abc+1])
    person_address =''
    for x in trans_en_split:
        if 'Address' in x:
            abc=trans_en_split.index(x)
            addres=trans_en_split[abc:abc+2]
            person_address=' '.join(x for x in addres)
            person_address=re.sub('[^A-Za-z0-9-/. ]','',person_address)
            print("address:",person_address)
  
    if person_address =='':
   
        for x in trans_en_split:
       
            if "Born" in x:
                abc=trans_en_split.index(x)
                addres=trans_en_split[abc+2:abc+4]
                person_address=' '.join(x for x in addres)
                person_address=re.sub('[^A-Za-z0-9-/. ]','',person_address)
                print("address1:",person_address)
    if person_address =='':
   
        for x in trans_en_split:
       
            if "Date" in x:
                abc=trans_en_split.index(x)
                addres=trans_en_split[abc+1:abc+3]
                person_address=' '.join(x for x in addres)
                person_address=re.sub('[^A-Za-z0-9-/. ]','',person_address)
                print("address2:",person_address)
    print("name:",name)
    print("last_name:",last_name)
    person_details ={"uid":uid,"date_of_birth":birth_date,"name":re.sub('[^A-Za-z. ]','',name),"last_name":re.sub('[^A-Za-z. ]','',last_name),"person_address":re.sub('[^A-Za-z0-9-/ ]','',person_address)}
    print(person_details)
    return person_details
detect_text('/home/raghu/Pictures/thaiid/46.jpeg')
#religious divide
#Buddhism
#religious
#Future
#at It