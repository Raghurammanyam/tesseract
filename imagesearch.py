import os
from os.path import join
import cv2
import numpy as np
import time as time
import scipy.spatial.distance as dist
# directory="D:\photo"
# searchImage="C:\image11.jpg"
index={}
def find(directory):
      for (dirname,dirs,files) in os.walk(directory):
          for filename in files:
              if (filename.endswith(".JPG")):
                  fullpath=join(dirname,filename)
                  index[fullpath]=features(fullpath)
 
      print("total number of photos in this directory %s"%len(index))
      return index
def features(imageDirectory):
       img=cv2.imread(imageDirectory)
       histogram=cv2.calcHist([img],[0,1,2],None,  [[8,8,8],256,0,256,0,256])
       Nhistogram=cv2.normalize(histogram)
       return Nhistogram.flatten()


def search(SearchImage,SearchDir):
     histim=histogramvalue(SearchImage)
     allimages=find(SearchDir)
     match=top(histim,allimages)
     return match
def top(histim,allimages):
      correlation={}
      for (address,value) in allimages.items():
        correlation[address]=cv2.compareHist(histim,value,cv2.cv.CV_COMP_CHISQR)
        ranked=sorted(correlation.items() ,key=lambda tup:       float(tup[1]))
        return ranked[0:10]
directory="/home/raghu/Downloads/idvoter"
searchImage="/home/raghu/Downloads/idvoter/2019-10-12 22:24:53.720765.jpg"
finalOutput=search(searchImage,directory)
for imageAdd,Histvalue in finalOutput:
    image=cv2.imread(imageAdd)
    resized=cv2.resize(image,(0,0),fx=0.25,fy=0.25)
    cv2.imshow("image directory %s %s"% (imageAdd,Histvalue),resized)
    cv2.waitKey(0)