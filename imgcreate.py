from pymongo import MongoClient
import os
import base64
import datetime
import random
from mongoengine import connect
#client =connect('mongodb://192.168.1.27:27017/passport-app')
client = connect('passport-app', host='192.168.1.27', port=27017)

# change database to localdb
# client=MongoClient('mongodb://192.168.1.27:27017/passport-app')
print(client)
mydb = client["passport-app"]
guests = list(mydb.guests.aggregate([
    {'$match':
     {'documentType': 'aadhaar',
      'localIdBackImage': {"$nin": ['null', '']},
      'localIdFrontImage':{"$nin": ['null', '']}}},
       {'$project': 
       {'localIdBackImage': 1,
        'localIdFrontImage': 1,
         'documentType': 1,
          'givenName': 1}}]))
print(guests)
for x in guests:
    # print(x['documentType'])
    doc_type = 'nonamed'
    name = str(random.randint(0, 9))
    if 'documentType' in x:
        doc_type = x['documentType']
        # print(doc_type)
    if 'localIdBackImage' in x:
        name = 'backImage'
    if 'localIdFrontImage' in x:
        name = 'frontImage'
    print(name)
    if os.path.isdir(doc_type) is not True:
        os.mkdir(doc_type)
    print(doc_type.encode("utf-8").decode(), name)
   # name = str(datetime.datetime.now())
    foldername = doc_type.encode("utf-8").decode() + "/" + name + ".jpg"
    # if 'passportImage' in x:
    #     with open(foldername, 'wb') as f:
    #         if x['passportImage'] is not None:
    #             img_data = base64.b64decode(x['passportImage'])
    #             f.write(img_data)
    # if 'visaImage' in x:
    #     with open(foldername, 'wb') as f:
    #         if x['visaImage'] is not None:
    #             img_data = base64.b64decode(x['visaImage'])
    #             f.write(img_data)
    if 'localIdFrontImage' in x:
        with open(foldername, 'wb') as f:
            if x['localIdFrontImage'] is not None:
                img_data = base64.b64decode(x['localIdFrontImage'])
                f.write(img_data)
    if 'localIdBackImage' in x:
        with open(foldername, 'wb') as f:
            if x['localIdBackImage'] is not None:
                img_data = base64.b64decode(x['localIdBackImage'])
                f.write(img_data)
    if 'nonamed' in x:
        with open(foldername, 'wb') as f:
            if x['nonamed'] is not None:
                img_data = base64.b64decode(x['nonamed'])
                f.write(img_data)
