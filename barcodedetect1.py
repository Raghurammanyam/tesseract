
import numpy as np
import cv2
from PIL import Image, ImageDraw
image = cv2.imread("/home/raghu/xxxx88899rotate.jpeg")
orig_im = Image.open("/home/raghu/xxxx88899rotate.jpeg")
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# equalize lighting
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
gray = clahe.apply(gray)

# edge enhancement
edge_enh = cv2.Laplacian(gray, ddepth = cv2.CV_8U, 
                         ksize = 3, scale = 1, delta = 0)
cv2.imshow("Edges", edge_enh)
cv2.waitKey(0)
retval = cv2.imwrite("edge_enh.jpg", edge_enh)

# bilateral blur, which keeps edges
blurred = cv2.bilateralFilter(edge_enh, 13, 50, 50)

# use simple thresholding. adaptive thresholding might be more robust
(_, thresh) = cv2.threshold(blurred, 55, 55, cv2.THRESH_BINARY)
cv2.imshow("Thresholded", thresh)
cv2.waitKey(0)
retval = cv2.imwrite("thresh.jpg", thresh)

# do some morphology to isolate just the barcode blob
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 9))
closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
closed = cv2.erode(closed, None, iterations = 4)
closed = cv2.dilate(closed, None, iterations = 4)
cv2.imshow("After morphology", closed)
cv2.waitKey(0)


retval = cv2.imwrite("closed.jpg", closed)

# find contours left in the image
cnts = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
cnts = sorted(cnts, key = cv2.contourArea)
# rect = cv2.minAreaRect(c)
# print(rect)
# box = np.int0(cv2.boxPoints(rect))
H,W = image.shape[:2]
for cnt in cnts:
    x,y,w,h = cv2.boundingRect(cnt)
    if cv2.contourArea(cnt) > 100 and (0.7 < w/h < 1.3) and (W/4 < x + w//2 < W*3/4) and (H/4 < y + h//2 < H*3/4):
        print(kk)
mask = np.zeros(image.shape[:2],np.uint8)
cv2.drawContours(image, [cnt], -1, (0, 255, 0), 3)
# print(box)
# crop = image[box]
# cv2.imshow('Image', crop)
dst = cv2.bitwise_and(image, image, mask=mask)
# new_image=cv2.imcrop(image, rect)
# cv2.imwrite("founded.jpeg",rect)
# text_im = orig_im.crop(box)
# text_im.save("founded453.jpeg")
cv2.imshow("found barcode", dst)
cv2.waitKey(0)
retval = cv2.imwrite("found.jpg", dst)