import base64
import requests
import io
import os
import json
import re
import datetime
from datetime import date
from PIL import Image
from pprint import pprint
import cv2
from os.path import join
from collections import defaultdict
from difflib import get_close_matches
import dateparser
from collections import defaultdict, OrderedDict


def detect_text(text, doc_type):
    try:
        remove = ['GOVERNMENT OF INDIA', 'Government of India', 'Year of Birth',
                  '/ Male', 'GOVERNMENT OF IND', 'Nent of India', 'GOVERMENTER']
        unlike = ['UNIQUE IDENTIFICATION AUTHORITY', 'OF INDIA', 'Identification', 'Bengaluru-560001', '-500001', '500001', 'Bengaluru-580001', '560001', ' WWW', 'WWW', '-560001', '-560101', '560101', 'uidai', 'Aam Admi ka', 'VvV', 'he', 'uldai', 'uldal', 'govin', 'www', 'A Unique Identification', 'Www', 'in', 'gov', 'of India', 'uidai', 'INDIA', 'India', 'www', 'I', '1B 1ST', 'MERI PEHACHAN', '1E 1B',
                  'MERA AADHAAR', 'Unique Identification Authority', 'of India', 'UNQUE IDENTIFICATION AUTHORITY', '1800 180 1947', '1800180 1947', 'Admi ka Adhikar', 'w', 'ww', 'S', 's', '1800 180 17', 'WWW', 'dai', 'uidai', 'Address', '1809 180 1947', 'help', 'AADHAAR', '160 160 1947', 'Aadhaar', '180 18167', 'Aadhaar-Aam Admi ka Adhikar', 'gov in', '1947', 'MERA AADHAAR MERI PEHACHAN', '38059606 3964', '8587 1936 9174']
        # url = 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyAOztXTencncNtoRENa1E3I0jdgTR7IfL0'
        # header = {'Content-Type': 'application/json'}
        # body = {
        #     'requests': [{
        #         'image': {
        #             'content': image_file,
        #         },
        #         'features': [{
        #             'type': 'DOCUMENT_TEXT_DETECTION',
        #             'maxResults': 100,
        #         }],
        #         "imageContext":{
        #         "languageHints":["en-t-iO-handwrit"]
        #         }
        #     }]
        # }

        # response = requests.post(url, headers=header, json=body).json()
        # text = response['responses'][0]['textAnnotations'][0]['description'] if len(response['responses'][0]) > 0 else ''
        # print ("totaltext:",text)
        block = str(text).split('\n')
        if doc_type == 'front':
            # regex = re.compile('[^a-zA-Z0-9-/ ]')
            # text=regex.sub('', text)
            # print(text)
            abc = [str(x) for x in block]
            dob_in = re.compile(
                '[0-9]{4}|[0-9]{2}\/[0-9]{2}\/[0-9]{4}|[0-9]{2}\-[0-9]{2}\-[0-9]{4}|[0-9]{2}\/[0-9]{2}\/[0-9]+')
            date = dob_in.findall(text)
            # date_of_birth = date[0]
            print("date_of_birth:", date)
            for y in date:
                find_date = re.search(
                    r'([0-9]{2}\/[0-9]{2}\/[0-9]{4}|[0-9]{2}\-[0-9]{2}\-[0-9]{4}|[0-9]{2}\/[0-9]{2}\/[0-9]+)', y)
                if find_date:
                    date_of_birth = y
            #parsed_birth=dateparser.parse(date[0],settings={'DATE_ORDER': 'YMD'}).date()
            # print(parsed_birth,"parsed_birth")
            gender_list = ['MALE', 'FEMALE', 'Male', 'Female']
            gender = ''
            for x in block:
                for y in gender_list:
                    if y in x:
                        gender = y
                        print("getting gender:", y)
            da_find = re.compile(
                '([0-9]{2,4} [0-9]{2,4} [0-9]{2,4}|[0-9]+ [0-9]+|[0-9]{12})')
            number = da_find.findall(text)
            number = [x for x in number if len(x) > 6]
            uid = ''
            if len(number) > 0:
                uid = number[0]
                for x in number:
                    find_uid = re.search(r'([0-9]+ [0-9]+ [0-9]+)', x)
                    if find_uid:
                        if len(re.sub(' ', '', x)) is 12:
                            uid = x
                            print("uid:", number)
            if date_of_birth in uid:
                date_of_birth = ''
            na_find = re.compile(
                '([a-zA-Z]+ [a-zA-Z]+ [a-zA-Z]+|[a-zA-Z]+ [a-zA-Z]+|[a-zA-Z]+)')
            noun = na_find.findall(text)
            noun = [
                x for x in noun if x not in remove if 'GOVERNMENT' not in x if 'Government' not in x if 'Govern' not in x if 'ERNM' not in x if 'ernm' not in x if 'india' not in x if 'India' not in x if 'ndia' not in x if 'Gover' not in x if 'Your' not in x if 'we ow' not in x if 'ae ae' not in x if 'India' not in x if 'INDIA' not in x if 'ee ee' not in x if 'OVERN' not in x if 'of' not in x if x not in gender if 'on ae ee' or 'GOVE' or 'TTT ATT' or 'PS ee' not in x if 'dhaar No' not in x if 'attr' not in x or 'we ow' not in x or 'ORR REE' not in x if 'IMEI' not in x or 'ramen' not in x or ' ee' not in x or ' ae' not in x or 'ndia' not in x if len(x) > 3]
            print(noun)
            name = ''
            if len(noun) > 0:
                name = noun[0]
            person_details = {"Date_of_birth": date_of_birth,
                              "sex": gender, "uid": uid, "name": name}
            return person_details
        elif doc_type == 'back':
            final_address = []
            for x in block:
                if 'Address' in x:
                    abc = block.index(x)
            # print(abc)
            address = block[abc:]
            # print(address,"before")
            regex = re.compile('([^a-zA-Z0-9-/ ]|Address|No|www|o  |uidai)')
            cannot = ([regex.sub('', i) for i in address])
            # print(cannot,"///after")
            #cannot = [x for x in cannot if len(x)>2 if 'No' not in x if 'o  ' not in x if 'www' not in x if 'Address' not in x]
            cannot = [x for x in cannot if x not in unlike]
            unique_list = list(OrderedDict((element, None)
                                           for element in cannot))
            for x in unique_list:
                abc = x.lstrip('  ')
                abc = x.lstrip(' -')
                abc = x.lstrip(' ')
                final_address.append(abc)
            for x in final_address:
                match = re.compile('(govin|ligovin|help)')
                abc = match.search(x)
                if abc:
                    index_match = final_address.index(x)
                    final_address.remove(x)
            for x in final_address:
                pin = re.search('([0-9]{6})', x)
                if pin:
                    ind = final_address.index(x)
            final_address = final_address[:ind+1]
            abc = ' '.join(x for x in final_address)
            final = abc.split()
            final_address = list(OrderedDict((element, None)
                                             for element in final))
            print("final_address:", final_address)
            person_address = ' '.join(x for x in final_address)
            return {'person_address': person_address}
    except IndexError as e:
        return ({"error": str(e)})
    except Exception as e:
        return ({"error": str(e)})
